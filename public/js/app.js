console.log('Client side JS file is loaded');

const weatherForm = document.querySelector('form');

const searchElement = document.querySelector('input');

const messageOne = document.querySelector('#message-1');
const messageTwo = document.querySelector('#message-2');

weatherForm.addEventListener('submit', (event) => {
  event.preventDefault();
  // To prevent the default behavior which is to refresh the browser allowing the server to render a new page,
  // Instead it will do nothing it will just allow us to do whatever we want by letting the fucntion run.
  console.log('Search button is clicked');

  const searchLocation = searchElement.value;

  console.log('Location provided = ' + searchLocation);

  const locationSearchURL = '/weather?address=' + searchLocation;

  messageOne.textContent = 'Loading...';
  messageTwo.textContent = '';

  // featch() is broser fucntion not JS function
  // It allows us to fetch data from URL and do something with it
  fetch(locationSearchURL).then((response) => {
    response.json().then((data) => {
      if (data.error) {
        //console.log(data.error);
        messageOne.textContent = data.error;
      } else {
        messageOne.textContent = data.location;
        messageTwo.textContent = data.forecast;
        //console.log(data.location);
        //console.log(data.forecast);
      }
    });
  });
});
