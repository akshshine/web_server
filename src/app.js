const path = require('path'); // Core path module to deal with file paths
const express = require('express'); // To load express library
const hbsModule = require('hbs');

const geocode = require('./utils/geocode');
const forecast = require('./utils/forecast');
const { error } = require('console');

//console.log(__dirname);
//console.log(path.join(__dirname, '../public')); // path.join is used to join paths together

const applicationPort = process.env.PORT || 3000    
// If we deploy our application on heroku, heroku will provide us port number automatically that we can fetch with help of process.env.PORT 
// Else while running on local machine it will take default port provide as 3000

// express Library which is loaded by require exposes single function called as express()
// we can call this express() function to create new express application

const app = express(); // Generate express application by calling express() function and store it into variable
// The express() function doesn't take in any arguments, instead we can configure our server by using various methods provided on the application (on app veriable)

// Defined paths for Express config
const publicDirectoryPath = path.join(__dirname, '../public');
const viewPath = path.join(__dirname, '../templates/views');
const partialsPath = path.join(__dirname, '../templates/partials');

// Setup handlebars engine and views location
app.set('view engine', 'hbs'); // This will set up Handlebars
// app.set(key,value) allows to set value for a given Express Setting
app.set('views', viewPath); // To set view directory path for view templates

hbsModule.registerPartials(partialsPath);

// Setup static directory to serve
app.use(express.static(publicDirectoryPath));

app.get('', (req, res) => {
  // render(view template , object) method is used to Handlebar template
  res.render('index', {
    title: 'Weather Application',
    name: 'Akshay Shinde'
  });
});

app.get('/about', (req, res) => {
  res.render('about', {
    title: 'About page',
    name: 'Akshay Shinde'
  });
});

app.get('/help', (req, res) => {
  res.render('help', {
    title: 'Help page',
    name: 'Akshay Shinde',
    helpText: 'This is some help text'
  });
});

app.get('/weather', (req, res) => {
  if (!req.query.address) {
    return res.send({
      error: 'You must provide address'
    });
  }

  // {} are used as default value while object destructuring
  geocode(req.query.address, (error, { lattitude, longitude, location } = {}) => {
    if (error) {
      return res.send({
        error // Using Obejct Shorthanding
      });
    }

    forecast(lattitude, longitude, (error, forecastData) => {
      if (error) {
        return res.send({
          error
        });
      }

      res.send({
        forecast: forecastData,
        location,
        address: req.query.address
      });
    });
  });
});

app.get('/products', (req, res) => {
  if (!req.query.search) {
    return res.send({
      error: 'You must provide a search term'
    });
  }

  console.log(req.query.search);
  res.send({
    products: []
  });
});

app.get('/help/*', (req, res) => {
  res.render('404', {
    title: '404',
    name: 'Akshay Shinde',
    errorMessage: 'Help article not found.'
  });
});

app.get('*', (req, res) => {
  res.render('404', {
    title: '404',
    name: 'Akshay Shinde',
    errorMessage: 'Page not found'
  });
});

// Since we are using express.static with path to find and load index.html commenting following default request anymore
/* 
 app.get('', (req, res) => {
   // root or default request
   res.send('<h1>Weather Application</h1>'); // This allows us to send something back to requester
 });
 */

// Since we are using express.static with path to find and load help.html commenting following /help request anymore
/* app.get('/help', (req, res) => {
  res.send({
    name: 'test',
    age: 27
  });
});


// Since we are using express.static with path to find and load about.html commenting following /about request anymore
app.get('/about', (req, res) => {
  res.send('<h2>About page</h2>');
});
*/

/*
// Adding wildCard character '*' to match every other request starting with '/help' apart from all request above
app.get('/help/*', (req, res) => {
  res.send('Help page not found');
});

// Adding 404 page using wildCard character '*' to match every other request apart from all request above
app.get('*', (req, res) => {
  res.send('My 404 page');
});

*/

// The app.listen() function is used to bind and listen to the connections on the specified host and port
// This method is identical to Node’s http.Server.listen() method
// If the port number is omitted or is 0, the operating system will assign an arbitrary unused port
app.listen(applicationPort, () => {
  console.log('Server started on the port '+applicationPort+'.');
});
