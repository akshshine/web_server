const request = require('request');

// const weatherUrl =
//   'http://api.weatherstack.com/current?access_key=da12b8885cc7fe16b9609bf156c7f0b2&query=37.8267,-122.4233&units=f'

// request({ url: weatherUrl, json: true }, (error, response) => {
//   if (error) {
//     console.log('Unable to connect to Weather Service!')
//   } else if (response.body.error) {
//     console.log('Unable to find the location')
//   } else {
//     console.log(
//       response.body.current.weather_descriptions[0] +
//         '. It is currently ' +
//         response.body.current.temperature +
//         ' degrees fahrenheit out. It feels like ' +
//         response.body.current.feelslike +
//         ' degrees fahrenheit out.'
//     )
//   }
// })

const forecast = (lattitude, longitude, callback) => {
  const url =
    'http://api.weatherstack.com/current?access_key=da12b8885cc7fe16b9609bf156c7f0b2&query=' +
    lattitude +
    ',' +
    longitude +
    '&units=f';

  request({ url, json: true }, (error, { body }) => {
    if (error) {
      callback('Unable to connect to Weather Service!', undefined);
    } else if (body.error) {
      callback('Unable to find the location', undefined);
    } else {
      callback(
        undefined,
        body.current.weather_descriptions[0] +
          '. It is currently ' +
          body.current.temperature +
          ' degrees fahrenheit out. It feels like ' +
          body.current.feelslike +
          ' degrees fahrenheit out. The humidity is '+body.current.humidity + '%.'
      );
    }
  });
};

module.exports = forecast;
