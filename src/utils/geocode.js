const request = require('request');

// const geocodingUrl =
//   'https://api.mapbox.com/geocoding/v5/mapbox.places/Los%20Angeles.json?access_token=pk.eyJ1IjoiYWtzaHNoaW5lIiwiYSI6ImNsbWc0dHpocDA5dTEzdGxtdng1djI0emwifQ.sBmIzKpaveyXfrYReilvQg&limit=1'

// request({ url: geocodingUrl, json: true }, (error, response) => {
//   if (error) {
//     console.log('Unable to connect to Location Service!')
//   } else if (response.body.features.length === 0) {
//     console.log(
//       'Unable to find the location. Try again with different search term'
//     )
//   } else {
//     const longitude = response.body.features[0].center[0]
//     const lattitude = response.body.features[0].center[1]
//     console.log('longitude= ' + longitude)
//     console.log('lattitude= ' + lattitude)
//   }
// })

const geocode = (address, callback) => {
  // encodeURIComponent is used handle special character inside address which actually means something in URL structure

  const url =
    'https://api.mapbox.com/geocoding/v5/mapbox.places/' +
    encodeURIComponent(address) +
    '.json?access_token=pk.eyJ1IjoiYWtzaHNoaW5lIiwiYSI6ImNsbWc0dHpocDA5dTEzdGxtdng1djI0emwifQ.sBmIzKpaveyXfrYReilvQg&limit=1';

  request({ url, json: true }, (error, { body }) => {
    if (error) {
      callback('Unable to connect to Location Services!', undefined);
    } else if (body.features.length === 0) {
      callback('Unable to find the location. Try again with different search term', undefined);
    } else {
      const resultData = {
        location: body.features[0].place_name,
        longitude: body.features[0].center[0],
        lattitude: body.features[0].center[1]
      };

      callback(undefined, resultData);
    }
  });
};

module.exports = geocode;
